import os,sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
fontsze = 8
plt.rcParams['figure.autolayout'] = True
plt.rcParams['font.family'] = 'Times New Roman'
plt.rcParams.update({'font.size': fontsze,
                     'mathtext.fontset': 'stix'})
plt.rc('xtick',labelsize=8)
plt.rc('ytick', labelsize=8)
plt.rc('axes',labelsize=fontsze)
plt.rc('axes',titlesize=fontsze)
plt.rcParams["text.latex.preamble"] += r"""
\usepackage{amsmath}
"""


SHOW_FIG = False
SAVE_FIG = True
if SHOW_FIG:
  assert not SAVE_FIG
if SAVE_FIG:
  assert not SHOW_FIG




def sq_v_st_order_param(Lx, Ly, d, Rbs, inter='LR', ordp="star"):
  """Computes the order parameter specified by the "ordp" kwarg. Lx and Ly specify the
     system size. "d" is the value of the delta parameter in the hamiltonian. "Rbs" is
     a list of Rb parameter values to compute for the given delta value. The "inter"
     kwarg specifices whether we are plotting calculation done with all long-range
     interactions, or truncated interactions beyond a distance of 2. Valid values for
     the "inter" and "ordp" kwargs are given in the assert statements at the beginning
     of the function."""

  assert inter in ['LR', 'trunc']
  assert ordp in ["star", "star_lukin", "square", "sqst_lukin", "striated", "checker_lukin"]

  order_p = []
  for Rb in Rbs:
    profile = extract_dens_profile(Lx, Ly, d, Rb, inter)

    if ordp == "star":
      summer = 0
      for x in range(Lx):
        for y in range(Ly):
          summer += (profile[y,x] - profile[x,y])**2
      order_p.append(summer  / (Lx*Ly))

    elif ordp == "star_lukin":
      summer = 0
      for x in range(Lx):
        for y in range(Ly):
          summer += 0.5 * np.exp(1j * (np.pi * x + np.pi/2*y)) * profile[y,x]
          summer += 0.5 * np.exp(1j * (np.pi * y + np.pi/2*x)) * profile[y,x]
      order_p.append(summer / (Lx*Ly))

    elif ordp == "square":
      summer = 0
      for x in range(Lx):
        for y in range(Ly):
          if x < Lx-1 and y < Ly-1:
            if x%2 == 0 and y%2 == 0:
              summer += profile[y,x]
            else:
              summer -= profile[y,x]
      order_p.append(summer  / ((Lx-1)*(Ly-1)))

    elif ordp == "sqst_lukin":
      summer = 0
      for x in range(Lx):
        for y in range(Ly):
          summer += 0.5 * np.exp(1j * (np.pi * x + 0*y)) * profile[y,x]
          summer += 0.5 * np.exp(1j * (np.pi * y + 0*x)) * profile[y,x]
          summer -= 0.5 * np.exp(1j * (np.pi/2 * x + np.pi*y)) * profile[y,x]
          summer -= 0.5 * np.exp(1j * (np.pi/2 * y + np.pi*x)) * profile[y,x]
      order_p.append(summer / (Lx*Ly))

    elif ordp == "striated":
      summer = 0
      for x in range(Lx):
        for y in range(Ly):
          if x+1 < Lx-1 and x > 0 and y+1 < Ly-1 and y > 0:
            summer += profile[y,x] * profile[y+1,x+1] * abs(profile[y,x] - profile[y+1,x+1])
      order_p.append(summer / ((Lx-1)*(Ly-1)))

    elif ordp == "checker_lukin":
      summer = 0
      for x in range(Lx):
        for y in range(Ly):
          summer += 0.5 * np.exp(1j * (np.pi * x + np.pi*y)) * profile[y,x]
          summer += 0.5 * np.exp(1j * (np.pi * y + np.pi*x)) * profile[y,x]
          summer -= 0.5 * np.exp(1j * (0 * x + np.pi*y)) * profile[y,x]
          summer -= 0.5 * np.exp(1j * (0 * y + np.pi*x)) * profile[y,x]
      order_p.append(summer / (Lx*Ly))


  return order_p


def extract_dens_profile(Lx, Ly, d, Rb, inter='LR'):
  """Extract the density profile from the raw data files, given the system size,
     delta, Rb, and interaction cutoff values. See above function for specifics about
     these arguments."""

  assert inter in ['LR', 'trunc']
  fname = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}.out"

  profile = np.zeros([Ly,Lx])

  if Lx == 9:
    E1 = None
    fname1 = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}_star.out"
    try:
      with open(fname1,"r") as f:
        lines = f.readlines()
        for line in lines:
          if "Final energy" in line: E1 = float(line.split(" ")[-1])
    except: pass

    E2 = None
    fname2 = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}_square.out"
    try:
      with open(fname2,"r") as f:
        lines = f.readlines()
        for line in lines:
          if "Final energy" in line: E2 = float(line.split(" ")[-1])
    except: pass

    E3 = None
    fname3 = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}_starfrus.out"
    try:
      with open(fname3,"r") as f:
        lines = f.readlines()
        for line in lines:
          if "Final energy" in line: E3 = float(line.split(" ")[-1])
    except: pass

    if E1 is None and E2 is None and E3 is None:
      raise ValueError(f"No final energy for parameter values d={d}, Rb={Rb}")
    elif E3 is None and E2 is None:
      fname = fname1
    elif E3 is None and E1 is None:
      fname = fname2
    elif E2 is None and E1 is None:
      fname = fname3
    elif E3 is None:
      if E1 < E2: fname = fname1
      else: fname = fname2
    elif E2 is None:
      if E1 < E3: fname = fname1
      else: fname = fname3
    elif E1 is None:
      if E2 < E3: fname = fname2
      else: fname = fname3
    else:
      if E1 < E2 and E1 < E3: fname = fname1
      elif E3 < E1 and E3 < E2: fname = fname3
      else: fname = fname2



  elif Lx == 13:
    E1 = None
    fname1 = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}_star.out"
    try:
      with open(fname1,"r") as f:
        lines = f.readlines()
        for line in lines:
          if "Final energy" in line: E1 = float(line.split(" ")[-1])
    except: pass

    E2 = None
    fname2 = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}_square.out"
    try:
      with open(fname2,"r") as f:
        lines = f.readlines()
        for line in lines:
          if "Final energy" in line: E2 = float(line.split(" ")[-1])
    except: pass

    E3 = None
    fname3 = f"data/{Lx}x{Ly}_{d}_{Rb}_{inter}_starfrus.out"
    try:
      with open(fname3,"r") as f:
        lines = f.readlines()
        for line in lines:
          if "Final energy" in line: E3 = float(line.split(" ")[-1])
    except:pass

    if E1 is None and E2 is None and E3 is None:
      raise ValueError(f"No final energy for parameter values d={d}, Rb={Rb}")
    elif E3 is None and E2 is None:
      fname = fname1
    elif E3 is None and E1 is None:
      fname = fname2
    elif E2 is None and E1 is None:
      fname = fname3
    elif E3 is None:
      if E1 < E2: fname = fname1
      else: fname = fname2
    elif E2 is None:
      if E1 < E3: fname = fname1
      else: fname = fname3
    elif E1 is None:
      if E2 < E3: fname = fname2
      else: fname = fname3
    else:
      if E1 < E2 and E1 < E3: fname = fname1
      elif E3 < E1 and E3 < E2: fname = fname3
      else: fname = fname2



  try:
    with open(fname, 'r') as f:
        lines = f.readlines()
        for idx,line in enumerate(lines):
          if f"Analyzing occupation" in line:
            reading = True
            while reading:
              idx += 1
              try: line = lines[idx]
              except: break
              if "site" not in line:
                reading = False
                break
              loc = int(float(line.split(" ")[1][:-1]))
              dens = float(line.split(" ")[-1])
              for x in range(Lx):
                for y in range(Ly):
                  if y + x*Ly + 1 == loc:
                    profile[y,x] = dens
                    break
  except:
    pass

  return profile



def gen_all_diagrams():
  """Plot all the phase diagram panels for data computed in this publication. Experimental
     data is from Ebadi, et al. Quantum phases of matter on a 256-atom programmable quantum
     simulator. Nature 595 227-232 (2021)."""

  ds = np.around(np.arange(1.9,5.0,0.3), decimals=1)
  Rbs = np.around(np.arange(1.2,2.05,0.1), decimals=1)

  fig, axm = plt.subplots(nrows=4, ncols=4, sharex='none', sharey='row', figsize=(5.1,5.1),
                          gridspec_kw={'height_ratios':[1,0.1,1,1]})
  handles = []




  ax = axm[3,0]
  ax.set_ylabel(r"$R_b$")
  ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi) - \tilde{\mathcal{F}}(\pi, 0)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='LR', ordp="checker_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0-0.02, vmax=0.25+0.02)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")
  ax.text(1.0, 1.0, "9x9 long-range", color='w')


  ax = axm[3,1]
  ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, 0) - \tilde{\mathcal{F}}(\pi/2, \pi)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='LR', ordp="sqst_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0+0.01, vmax=0.19+0.01)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")


  ax = axm[3,2]
  ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='LR', ordp="star_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0, vmax=0.15)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")


  ax = axm[3,3]
  ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='LR', ordp="star")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0, vmax=0.2)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")




  ax = axm[0,0]
  ax.set_ylabel(r"$R_b$")
  #ax.set_xlabel(r"$\delta / \Omega$")
  ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi) - \tilde{\mathcal{F}}(\pi, 0)$", y=1.25)
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(13, 13, d, Rbs, inter='LR', ordp="checker_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  im = ax.pcolormesh(big_diagram, vmin=0.0-0.02, vmax=0.25+0.02)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")
  ax.text(1.0, 1.0, "13x13 long-range", color='w')

  axins = inset_axes(ax,
                    width="100%",  
                    height="5%",
                    loc='upper center',
                    borderpad=-0.8
                   )
  plt.colorbar(im, cax=axins, orientation="horizontal", ticks=[0.0, 0.25])
  axins.xaxis.set_label_position('top')
  axins.xaxis.set_ticks_position('top')


  ax = axm[0,1]
  #ax.set_xlabel(r"$\delta / \Omega$")
  ax.set_title(r"$\tilde{\mathcal{F}}(\pi, 0) - \tilde{\mathcal{F}}(\pi/2, \pi)$", y=1.25)
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(13, 13, d, Rbs, inter='LR', ordp="sqst_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  im = ax.pcolormesh(big_diagram, vmin=0.0+0.01, vmax=0.19+0.01)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")

  axins = inset_axes(ax,
                    width="100%",  
                    height="5%",
                    loc='upper center',
                    borderpad=-0.8
                   )
  plt.colorbar(im, cax=axins, orientation="horizontal", ticks=[0.02, 0.19])
  axins.xaxis.set_label_position('top')
  axins.xaxis.set_ticks_position('top')



  ax = axm[0,2]
  #ax.set_xlabel(r"$\delta / \Omega$")
  ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$", y=1.25)
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(13, 13, d, Rbs, inter='LR', ordp="star_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  im = ax.pcolormesh(big_diagram, vmin=0.01-0.005, vmax=0.06+0.0025)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")

  axins = inset_axes(ax,
                    width="100%",  
                    height="5%",
                    loc='upper center',
                    borderpad=-0.8 
                   )
  plt.colorbar(im, cax=axins, orientation="horizontal", ticks=[0.01, 0.06])
  axins.xaxis.set_label_position('top')
  axins.xaxis.set_ticks_position('top')



  ax = axm[0,3]
  #ax.set_xlabel(r"$\delta / \Omega$")
  ax.set_title(r"$\sum_{x,y} (\langle \hat{n}_{x,y}\rangle - \langle \hat{n}_{y,x} \rangle)^2 / N$", y=1.25)
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(13, 13, d, Rbs, inter='LR', ordp="star")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  im = ax.pcolormesh(big_diagram, vmin=0.0, vmax=0.2)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")

  axins = inset_axes(ax,
                    width="100%",  
                    height="5%",
                    loc='upper center',
                    borderpad=-0.8 
                   )
  plt.colorbar(im, cax=axins, orientation="horizontal", ticks=[0, 0.2], format="%.1f")
  axins.xaxis.set_label_position('top')
  axins.xaxis.set_ticks_position('top')





  ax = axm[2,0]
  #ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$")
  ax.set_ylabel(r"$R_b$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='trunc', ordp="checker_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0-0.02, vmax=0.25+0.02)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")
  ax.text(1.0, 1.0, "9x9 truncated", color='w')


  ax = axm[2,1]
  #ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='trunc', ordp="sqst_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0+0.01, vmax=0.19+0.01)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")


  ax = axm[2,2]
  #ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='trunc', ordp="star_lukin")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  im = ax.pcolormesh(big_diagram, vmin=0.0, vmax=0.15)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")

  axins = inset_axes(ax,
                    width="100%",  
                    height="5%",
                    loc='upper center',
                    borderpad=-0.8 
                   )
  cbar = plt.colorbar(im, cax=axins, orientation="horizontal", ticks=[0, 0.15], format="%.2f")
  #cbar.ax.set_xticklabels([])
  cbar.ax.xaxis.set_label_position('top')
  cbar.ax.xaxis.set_ticks_position('top')


  ax = axm[1,0]
  ax.axis('off')


  ax = axm[1,1]
  ax.axis('off')

  ax = axm[1,2]
  ax.axis('off')

  ax = axm[1,3]
  ax.axis('off')

  ax = axm[2,3]
  #ax.set_xlabel(r"$\delta / \Omega$")
  #ax.set_title(r"$\tilde{\mathcal{F}}(\pi, \pi/2)$")
  diagram = np.zeros((len(Rbs), len(ds)))
  for idx, d in enumerate(ds):
    diagram[:,idx] = sq_v_st_order_param(9, 9, d, Rbs, inter='trunc', ordp="star")
  xs = np.around(np.arange(4.9, -2.0, -0.3), decimals=1)
  xs = np.flip(xs)
  ys = np.around(np.arange(1.0, 2.05, 0.1), decimals=1)
  big_diagram = np.zeros((len(ys), len(xs)))
  big_diagram[2:, 13:] = diagram[:,:]
  ax.pcolormesh(big_diagram, vmin=0.0, vmax=0.2)

  ax.set_yticks([0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5])
  ax.set_yticklabels(["1", " ", "1.2", " " , "1.4", " ", "1.6", " ", "1.8", " ", "2"])
  ax.set_xticks([0.5, 3.8, 7.2, 10.5, 13.8, 17.2, 20.5])
  #ax.set_xticklabels(["-2", " ", "0", " ", "2", " ", "4"])
  ax.set_xticklabels([])
  ax.plot([13, 24], [2,2], color='cyan', linestyle="--")
  ax.plot([13,13], [2,11], color='cyan', linestyle="--")


  if SHOW_FIG:
    plt.show()
  if SAVE_FIG:
    plt.savefig('expt_compare_big.pdf', dpi=1000)












if __name__ == "__main__":

  gen_all_diagrams()
