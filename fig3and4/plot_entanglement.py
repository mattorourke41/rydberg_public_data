import os,sys
import numpy as np
import matplotlib.pyplot as plt
fontsze = 2*8
plt.rcParams['figure.autolayout'] = True
plt.rcParams['font.family'] = 'Times New Roman'
plt.rcParams.update({'font.size': fontsze,
                     'mathtext.fontset': 'stix'})
plt.rc('xtick',labelsize=2*8)
plt.rc('ytick', labelsize=2*8)
plt.rc('axes',labelsize=fontsze)
plt.rc('axes',titlesize=fontsze)


SHOW_FIG = False
SAVE_FIG = True
if SHOW_FIG:
  assert not SAVE_FIG
if SAVE_FIG:
  assert not SHOW_FIG



def extract_bondwise_EE(d, Rb, Lx, Ly, stub=None, fname=None):
  EE = []
  with open(fname, "r") as f:
      lines = f.readlines()
      reading = False
      for idx, line in enumerate(lines):
        if "Analyzing vN entanglement" in line:
          reading = True
          continue
        if reading == True and "Across bond" not in line:
          reading = False
          break
        if reading == True and "Across bond" in line:
          EE.append(float(line.split(" ")[-1]))

  return EE




if __name__ == "__main__":
  fig = plt.figure(figsize=(6.0,4.15))
  ax = fig.gca()
  ax.set_xlabel('partition location')
  ax.set_ylabel(r"von Neumann entropy")
  ax.set_ylim(0,1.95)
  ax.set_yscale('linear')
  EEs = extract_bondwise_EE(5.0, 2.3, 6, 9, fname = 'data/pbc/6colx9_D800_delta5.0_Rb2.3_EEspec.out')
  EEs += [0]  # to make the end of the plot look nicer
  ax.plot(list(range(len(EEs)-1)) + [len(EEs)+8], EEs, linestyle='-', lw=3.0, color='k')

  for x in np.around(np.arange(9,107,9), decimals=0):
    ax.axvline(x-1, linestyle='--', color='k', alpha=0.25)

  if SHOW_FIG: plt.show()
  if SAVE_FIG: plt.savefig("nematic_bondwise_EEspectrum.pdf", dpi=750)






  fig = plt.figure(figsize=(1.9,4.15))
  # these are the discrete Schmidt eigenvalues across bond 49
  EEspec = [0.4562, 0.2313, 0.2312, 0.0282, 0.0282, 0.0120, 0.0036, 0.0036, 0.0008]
  ax = fig.gca()
  ax.yaxis.tick_right()
  ax.yaxis.set_label_position("right")
  ax.set_yscale('log')
  ax.set_ylabel('Schmidt eigenvalue')
  ax.set_ylim(1e-4, 2)
  ax.set_xlim(0,1)
  ax.set_xticks([])
  for val in [EEspec[0], EEspec[5], EEspec[8]]:
    ax.plot([0.4,0.6], [val]*2, color='k')
  for val in [EEspec[1], EEspec[3], EEspec[6]]:
    ax.plot([0.25,0.45], [val]*2, color='k')
    ax.plot([0.55,0.75], [val]*2, color='k')

  if SHOW_FIG: plt.show()
  if SAVE_FIG: plt.savefig("nematic_bondwise_EElevels.pdf", dpi=750)






