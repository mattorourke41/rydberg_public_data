plot_correlations.py generates figures 3a,b and 4d,e. The data for the correlation functions
of the gamma-point quantum solution and mean field solutions are included in the script,
and the data for the 15x14 finite lattice calculation (density and correlations)
is given in data/edge_defects/.

corr_scaper.py is a helper function to read the data in data/edge_defects/

plot_entanglement.py generates figure 3d with the data in data/pbc/
