import os,sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from corr_scraper import corr_scraper
fontsze = 2*9
plt.rcParams['figure.autolayout'] = True
plt.rcParams['font.family'] = 'Times New Roman'
plt.rcParams.update({'font.size': fontsze,
                     'mathtext.fontset': 'stix'})
plt.rc('xtick',labelsize=2*8)
plt.rc('ytick', labelsize=2*8)
plt.rc('axes',labelsize=fontsze)
plt.rc('axes',titlesize=fontsze)


SHOW_FIG = False
SAVE_FIG = True
if SHOW_FIG:
  assert not SAVE_FIG
if SAVE_FIG:
  assert not SHOW_FIG






# correlation plots

def plot_pill_corrs(include_degeneracy=False, include_3star=False, include_finite=False):
    include_mf = True
    if include_finite:
      include_mf = False

    # aggregate correlations for quantum nematic phase (using 6col x 9, d=5.0, Rb=2.3)
    pillq_corr = {}
    pillq_corr[1.0] = [0.0]*4
    pillq_corr[np.sqrt(2)] = [0.0]*4
    pillq_corr[2.0] = [0.0]*4
    pillq_corr[np.sqrt(5)] = [0.0]*4 + [0.136]*4
    pillq_corr[np.sqrt(8)] = [0.136]*4
    pillq_corr[3.0] = [0.268]*2 + [0.0]*2
    pillq_corr[np.sqrt(10)] = [0.0]*8
    pillq_corr[np.sqrt(13)] = [0.0]*8
    pillq_corr[4.0] = [0.204]*2 + [0.0]*2
    pillq_corr[np.sqrt(17)] = [0.035]*4 + [0.0]*4
    pillq_corr[np.sqrt(18)] = [0.0]*4
    pillq_corr[np.sqrt(20)] = [0.136]*4 + [0.035]*4
    pillq_corr[5.0] = [0.204]*4 + [0.0]*8

    pillq_degens = {}
    pillq_degens[(np.sqrt(5),0.136)] = '4'
    pillq_degens[(np.sqrt(8),0.136)] = '4'
    pillq_degens[(3.0,0.268)] = '2'
    pillq_degens[(4.0,0.204)] = '2'
    pillq_degens[(np.sqrt(17),0.035)] = '4'
    pillq_degens[(np.sqrt(20),0.136)] = '4'
    pillq_degens[(np.sqrt(20),0.035)] = '4'
    pillq_degens[(5.0,0.204)] = '4'

    # aggregate correlations for mean field "nematic" ababab phase (using d=5.0, Rb=2.3)
    pillmf_corr = {}
    pillmf_corr[1.0] = [0.0]*4
    pillmf_corr[np.sqrt(2)] = [0.0]*4
    pillmf_corr[2.0] = [0.0]*4
    pillmf_corr[np.sqrt(5)] = [0.852,0.852] + [0.0]*6
    pillmf_corr[np.sqrt(8)] = [0.852,0.852] + [0.0]*2
    pillmf_corr[3.0] = [0.846,0.846] + [0.0]*2
    pillmf_corr[np.sqrt(10)] = [0.0]*8
    pillmf_corr[np.sqrt(13)] = [0.0]*8
    pillmf_corr[4.0] = [0.846,0.846] + [0.0]*2
    pillmf_corr[np.sqrt(17)] = [0.0]*8
    pillmf_corr[np.sqrt(18)] = [0.0]*4
    pillmf_corr[np.sqrt(20)] = [0.852,0.852] + [0.0]*6
    pillmf_corr[5.0] = [0.846,0.846,0.846,0.846] + [0.0]*8

    pillmf_degens = {}
    pillmf_degens[(np.sqrt(5),0.852)] = '2'
    pillmf_degens[(np.sqrt(8),0.852)] = '2'
    pillmf_degens[(3.0,0.846)] = '2'
    pillmf_degens[(4.0,0.846)] = '2'
    pillmf_degens[(np.sqrt(20),0.852)] = '2'
    pillmf_degens[(5.0,0.846)] = '4'



    # aggregate correlations for mean field 3star phase (using d=5.0, Rb=2.3)
    starmf_corr = {}
    starmf_corr[1.0] = [0.0]*4
    starmf_corr[np.sqrt(2)] = [0.0]*4
    starmf_corr[2.0] = [0.0]*4
    starmf_corr[np.sqrt(5)] = [0.896,0.896] + [0.0]*6
    starmf_corr[np.sqrt(8)] = [0.896,0.896] + [0.0]*2
    starmf_corr[3.0] = [0.896,0.896] + [0.0]*2
    starmf_corr[np.sqrt(10)] = [0.0]*8
    starmf_corr[np.sqrt(13)] = [0.0]*8
    starmf_corr[4.0] = [0.0]*4
    starmf_corr[np.sqrt(17)] = [0.896,0.896] + [0.0]*6
    starmf_corr[np.sqrt(18)] = [0.0]*4
    starmf_corr[np.sqrt(20)] = [0.896]*4 + [0.0]*4
    starmf_corr[5.0] = [0.0]*12

    starmf_degens = {}
    starmf_degens[(np.sqrt(5),0.896)] = '2'
    starmf_degens[(np.sqrt(8),0.896)] = '2'
    starmf_degens[(3.0,0.896)] = '2'
    starmf_degens[(np.sqrt(17),0.896)] = '2'
    starmf_degens[(np.sqrt(20),0.896)] = '4'



    plt.figure(figsize=(6.0,3.0))
    if not include_degeneracy:
      for (rad, corr) in pillq_corr.items():
        rads = [rad]*len(corr)
        plt.scatter(rads, corr, color='r', label='entangled' if rad == 1.0 else "", zorder=3, alpha=1.0)

      for (rad, corr) in pillmf_corr.items():
        rads = [rad] * len(corr)
        plt.scatter(rads, corr, color='b', marker='x', label='mean field' if rad == 1.0 else "", zorder=3, alpha=1.0)



    if include_degeneracy:
      for (rad, corr) in pillq_corr.items():
        corr = np.array(corr)
        rads = [rad]*len(corr[corr == 0])
        plt.scatter(rads, corr[corr==0], color='r', marker='.', zorder=3, alpha=0.2, s=64)
      for (rad, corr) in starmf_corr.items():
        corr = np.array(corr)
        rads = [rad] * len(corr[corr==0])
        if include_mf: plt.scatter(rads, corr[corr==0], color='b', marker='.', zorder=3, alpha=0.1, s=64)
      for (rad, corr) in pillmf_corr.items():
        corr = np.array(corr)
        rads = [rad] * len(corr[corr==0])
        if include_mf: plt.scatter(rads, corr[corr==0], color='g', marker='.', zorder=3, alpha=0.1, s=64)


      for coords, lab in pillmf_degens.items():
        if lab == '2' and include_mf:
          plt.scatter([coords[0]-0.033, coords[0]+0.033], [coords[1],coords[1]], color='g', marker='.', s=64)
          plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        elif lab == '4' and include_mf:
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1], [coords[1]]*4, color='g', marker='.', s=64)
          plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        elif include_mf:
          raise ValueError(f"unexpected degeneracy: {lab}")

      for coords, lab in starmf_degens.items():
        if lab == '2' and include_mf:
          plt.scatter([coords[0]-0.033, coords[0]+0.033], [coords[1],coords[1]], color='b', marker='.', s=64)
        elif lab == '4' and include_mf:
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1], [coords[1]]*4, color='b', marker='.', s=64)
        elif include_mf:
          raise ValueError(f"unexpected degeneracy: {lab}")
      plt.axvline(np.sqrt(17), color='k', alpha=0.3, linestyle='--')

      for coords, lab in pillq_degens.items():
        if lab == '2':
          plt.scatter([coords[0]-0.033, coords[0]+0.033], [coords[1],coords[1]], color='r', marker='.', s=64)
          if not include_mf: plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        elif lab == '4':
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1], [coords[1]]*4, color='r', marker='.', s=64)
          if not include_mf: plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        else:
          raise ValueError(f"unexpected degeneracy: {lab}")

      plt.scatter(1,2,color='r',marker='.',s=64, label='exact gs')
      if include_mf:
        plt.scatter(1,2,color='b',marker='.',s=64, label='mean field gs')
        plt.scatter(1,2,color='g',marker='.',s=64, label=r"mean field $3 \times 4$")



    if include_finite:
      corrs = corr_scraper(16,16, 104, "data/edge_defects/16x16_3.4_2.1_bolt_2.out")
      for rad, corr in corrs.items():
        corr = np.array(corr)
        if rad == 2.2361:
          rad = [rad, rad, rad-0.033, rad+0.033] + [rad]*4
          corr = sorted(corr)[::-1]
        elif rad == 2.8284:
          rad = [rad, rad, rad-0.033, rad+0.033]
          corr = sorted(corr)[::-1]
        elif rad == 3.0000:
          rad = [rad]*4
          corr = sorted(corr)[::-1]
        elif rad == 4.0000:
          rad = [rad]*4
          corr = sorted(corr)[::-1]
        elif rad == 4.1231:
          rad = [rad-0.033, rad+0.033, rad-0.033, rad+0.033] + [rad]*4
          corr = sorted(corr)[::-1]
        elif rad == 4.4721:
          rad = [rad, rad, rad-0.033, rad+0.033] + [rad]*4
          corr = sorted(corr)[::-1]
        elif rad == 5.0000:
          rad = [rad, rad+0.033, rad-0.033, rad] + [rad]*8
          corr = sorted(corr)[::-1]
        else:
          rad = [rad]*len(corr)

        plt.scatter(rad, corr, color='g', marker='.', s=64)

      plt.scatter(1,2,color='g',marker='.',s=64,label=r"finite $15 \times 14$")

    if not include_finite: plt.legend(loc='center left', fontsize=2*8, handletextpad=-0.1)
    else: plt.legend(loc='upper left', fontsize=2*8, handletextpad=-0.1)
    plt.xlabel(r"lattice distance $|\vec{r}_i - \vec{r}_j|$")
    plt.ylabel(r'correlation $\langle \hat{n}_i \hat{n}_j \rangle$')
    if include_mf:
      plt.ylim([-0.05, 1.0])
      plt.yticks([0.0,0.25,0.5,0.75], ['0.0', '0.25', '0.5', '0.75'])
    else:
      plt.ylim([-0.02, 0.3])
      plt.yticks([0,0.1,0.2,0.3], ['0.0', '0.1', '0.2', '0.3'])


    if SHOW_FIG:
      plt.show()
    if SAVE_FIG:
      if include_degeneracy:
        if include_mf: 
          #plt.savefig("nematic_mf_vs_q_corrs_DEGENS.png", dpi=750)
          plt.savefig("nematic_mf_vs_q_corrs_DEGENS.pdf", dpi=750)
        else:
          #plt.savefig("finite_nematic_15x14_corrs_DEGENS.png", dpi=750)
          plt.savefig("finite_nematic_15x14_corrs_DEGENS.pdf", dpi=750)
      else:
        #plt.savefig("nematic_mf_vs_q_corrs.png", dpi=450)
        plt.savefig("nematic_mf_vs_q_corrs.pdf", dpi=450)





def plot_striated_corrs(include_degeneracy=False):
    # aggregate correlations for quantum striated phase (using 8x8, d=3.1, Rb=1.5), site 10
    # corr values under 0.01 are recorded as 0.0
    stq_corr = {}
    stq_corr[1.0] = [0.0]*4
    stq_corr[np.sqrt(2)] = [.037]*4
    stq_corr[2.0] = [0.884]*4
    stq_corr[np.sqrt(5)] = [0.0]*8
    stq_corr[np.sqrt(8)] = [0.884]*4
    stq_corr[3.0] = [0.0]*4
    stq_corr[np.sqrt(10)] = [0.039]*8
    stq_corr[np.sqrt(13)] = [0.0]*8
    stq_corr[4.0] = [0.884]*4
    stq_corr[np.sqrt(17)] = [0.0]*8
    stq_corr[np.sqrt(18)] = [0.039]*4
    stq_corr[np.sqrt(20)] = [0.884]*8
    stq_corr[5.0] = [0.0]*12

    stq_degens = {}
    stq_degens[(np.sqrt(2),0.037)] = '4'
    stq_degens[(2.0,0.884)] = '4'
    stq_degens[(np.sqrt(8),0.884)] = '4'
    stq_degens[(np.sqrt(10),0.039)] = '8'
    stq_degens[(4.0,0.884)] = '4'
    stq_degens[(np.sqrt(18),0.039)] = '4'
    stq_degens[(np.sqrt(20),0.884)] = '8'


    # aggregate correlations for mean field striated phase (using d=5.0, Rb=2.3)
    stmf_corr = {}
    stmf_corr[1.0] = [0.0]*4
    stmf_corr[np.sqrt(2)] = [.034]*4
    stmf_corr[2.0] = [0.903]*4
    stmf_corr[np.sqrt(5)] = [0.0]*8
    stmf_corr[np.sqrt(8)] = [0.903]*4
    stmf_corr[3.0] = [0.0]*4
    stmf_corr[np.sqrt(10)] = [0.034]*8
    stmf_corr[np.sqrt(13)] = [0.0]*8
    stmf_corr[4.0] = [0.903]*4
    stmf_corr[np.sqrt(17)] = [0.0]*8
    stmf_corr[np.sqrt(18)] = [0.034]*4
    stmf_corr[np.sqrt(20)] = [0.903]*8
    stmf_corr[5.0] = [0.0]*12

    stmf_degens = {}
    stmf_degens[(np.sqrt(2),0.034)] = '4'
    stmf_degens[(2.0,0.903)] = '4'
    stmf_degens[(np.sqrt(8),0.903)] = '4'
    stmf_degens[(np.sqrt(10),0.034)] = '8'
    stmf_degens[(4.0,0.903)] = '4'
    stmf_degens[(np.sqrt(18),0.034)] = '4'
    stmf_degens[(np.sqrt(20),0.903)] = '8'


    plt.figure(figsize=(6.0,3.0))

    if not include_degeneracy:
      for (rad, corr) in stq_corr.items():
        rads = [rad]*len(corr)
        plt.scatter(rads, corr, color='r', label='entangled' if rad == 1.0 else "", zorder=3, alpha=1.0)

      for (rad, corr) in stmf_corr.items():
        rads = [rad] * len(corr)
        plt.scatter(rads, corr, color='b', marker='x', label='mean field' if rad == 1.0 else "", zorder=3, alpha=1.0)



    if include_degeneracy:
      for (rad, corr) in stq_corr.items():
        corr = np.array(corr)
        rads = [rad]*len(corr[corr == 0])
        plt.scatter(rads, corr[corr==0], color='r', marker='.', zorder=3, alpha=1.0, s=64)
      for (rad, corr) in stmf_corr.items():
        corr = np.array(corr)
        rads = [rad] * len(corr[corr==0])
        plt.scatter(rads, corr[corr==0], color='b', marker='x', zorder=3, alpha=1.0, s=8)

      for coords, lab in stq_degens.items():
        if lab == '2':
          plt.scatter([coords[0]-0.05, coords[0]+0.05], [coords[1],coords[1]], color='r', marker='o', s=25)
          plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        elif lab == '4':
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1], [coords[1]]*4, color='r', marker='.', s=64, alpha=1.0)
          plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        elif lab == '8':
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1]*2, [coords[1]+0.02]*4 + [coords[1]-0.02]*4, color='r', marker='.', s=64, alpha=1.0)
          plt.axvline(coords[0], color='k', alpha=0.3, linestyle='--')
        else:
          raise ValueError(f"unexpected degeneracy: {lab}")

      for coords, lab in stmf_degens.items():
        if lab == '2':
          plt.scatter([coords[0]-0.05, coords[0]+0.05], [coords[1],coords[1]], color='b', marker='x', s=8)
        elif lab == '4':
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1], [coords[1]]*4, color='b', marker='x', s=8, alpha=1.0)
        elif lab == '8':
          plt.scatter([coords[0]-0.1, coords[0]-0.033, coords[0]+0.033, coords[0]+0.1]*2, [coords[1]+0.02]*4 + [coords[1]-0.02]*4, color='b', marker='x', s=8, alpha=1.0)
        else:
          raise ValueError(f"unexpected degeneracy: {lab}")


    plt.scatter(1,2,color='r',marker='.',s=64, alpha=1.0, label="exact gs")
    plt.scatter(1,2,color='b',marker='x',s=8, alpha=1.0, label='mean field gs')
    plt.legend(loc='center left', fontsize=2*8, handletextpad=-0.1)
    plt.ylim([-0.05,0.95])
    plt.xlabel(r"lattice distance $|\vec{r}_i - \vec{r}_j|$")
    plt.ylabel(r'correlation $\langle \hat{n}_i \hat{n}_j \rangle$')
    plt.yticks([0.0,0.25,0.5,0.75], ['0.0', '0.25', '0.5', '0.75'])



    if SHOW_FIG:
      plt.show()
    if SAVE_FIG:
      if include_degeneracy:
        #plt.savefig("striated_mf_vs_q_corrs_DEGENS.png", dpi=750)
        plt.savefig("striated_mf_vs_q_corrs_DEGENS.pdf", dpi=750)
      else:
        #plt.savefig("striated_mf_vs_q_corrs.png", dpi=450)
        plt.savefig("striated_mf_vs_q_corrs.pdf", dpi=450)


def plot_15x14_edge_defect_density():
  fname = f"data/edge_defects/16x16_3.4_2.1_bolt_2.out"
  with open(fname, 'r') as f:
    lines = f.readlines()
    profile = np.zeros([16, 16])

    for idx,line in enumerate(lines):
      if f"Analyzing occupation" in line:
          reading = True
          while reading:
            idx += 1
            try: line = lines[idx]
            except: break
            if "site" not in line:
              reading = False
              break
            loc = int(float(line.split(" ")[1][:-1]))
            dens = float(line.split(" ")[-1])
            for x in range(16):
              for y in range(16):
                if y + x*16 + 1 == loc:
                  profile[y,x] = dens
                  break

  # plot the profile
  #profile = profile[:14,:15]
  plt.figure()
  plt.pcolormesh(profile)
  plt.clim(0.0,1.0)

  if SHOW_FIG:
    plt.show()
  if SAVE_FIG:
    plt.savefig("15x14_edge_defect_density.pdf", dpi=450)




if __name__ == "__main__":
  plot_striated_corrs(True)  # fig 3a
  plot_pill_corrs(include_degeneracy=True, include_finite=False) # fig 3b
  plot_pill_corrs(include_degeneracy=True, include_finite=True) # fig 4e
  plot_15x14_edge_defect_density() # fig 4d
