import os,sys
import numpy as np


def corr_scraper(Lx, Ly, init_site, fname, max_range=5):
  corrs = {}

  init_sites = []
  ctr_coord = (init_site//Ly, init_site%Ly - 1)
  #print(ctr_coord)
  for x in range(Lx):
    for y in range(Ly):
      coord = y + x*Ly + 1
      xdist = x - ctr_coord[0]
      ydist = y - ctr_coord[1]
      if (coord < init_site) and (np.sqrt(xdist**2 + ydist**2) <= max_range):
        init_sites.append(coord)

  with open(fname, 'r') as f:
    lines = f.readlines()
    reading = False
    for idx, line in enumerate(lines):
      if f"init site = {init_site}" in line:
        reading = True
        while reading:
          for inc in range(1,1000):
            if not ("dx" in lines[idx+inc]):
              reading = False
              break
            else:
              dist = float(lines[idx+inc].split(" ")[4][:-1])
              corr = float(lines[idx+inc].split(" ")[-1])
              if dist <= max_range:
                if not (dist in corrs): corrs[dist] = []
                corrs[dist].append(corr)

  with open(fname, 'r') as f:
    lines = f.readlines()
    line_idxs = []
    for idx, line in enumerate(lines):
      if f"init site =" in line:
        site = int(float(line.split(" ")[-1]))
        if site in init_sites: line_idxs.append(idx)

    for idx in line_idxs:
      line = lines[idx]
      site = int(float(line.split(" ")[-1]))
      assert site in init_sites

      sitexy = (site//Ly, site%Ly - 1)

      reading = True
      while reading:
          for inc in range(1,1000):
            if not ("dx" in lines[idx+inc]):
              reading = False
              break
            else:
              dx = float(lines[idx+inc].split(",")[0][3:])
              dy = float(lines[idx+inc].split(",")[1][4:])
              dist = float(lines[idx+inc].split(" ")[4][:-1])
              corr = float(lines[idx+inc].split(" ")[-1])
              if (dist <= max_range) and (dx == ctr_coord[0] - sitexy[0]) and (dy == ctr_coord[1] - sitexy[1]):
                if not (dist in corrs): corrs[dist] = []
                corrs[dist].append(corr)

  return corrs




