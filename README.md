This repository contains raw data and plotting scripts for some figures in the paper
"Entanglement in the quantum phases of an unfrustrated Rydberg atom array"
by Matthew O'Rourke and Garnet Chan. Specifically, this repo includes figures 3a, 3b, 3d,
4d, 4e, and all panels of figure 5.
The preprint for this paper can be found on
arxiv.org at arXiv:2201.03189. The published version can be found at XXX. If you use
this data, please appropriately cite the published version.

The plotting scripts require a working distribution of python3.6+ with matplotlib, numpy, etc.

If you are interested in obtaining data for the (sub-)figures not included in this repository, 
you may request it from the authors at: mattorourke41@gmail.com or gkc1000@gmail.com.


